size_x_between_vertices = 60;
size_y_between_vertices = 80;
rad_vert = 18;
start_x = 30;
start_y = 30;
default_delay = 50;
width_heavy = 4;
width_light = 2;
max_len = 99
calculated_sizes = false;
sd_len = 0.15;

if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

function rand_int(mx) {
	return Math.floor(Math.random() * mx);
}

function is_int_txt(txt) {
	if (txt == "") {
		return false;
	}
	for (var i = 0; i < txt.length; ++i) {
		if ('0' <= txt[i] && txt[i] <= '9') {
			continue;
		}
		return false;
	}	
	return true;
}

function is_int(name) {
	var elem = document.getElementsByName(name)[0];
	var txt = elem.value;
	return is_int_txt(txt);
}

function get_field(name) {
	var elem = document.getElementsByName(name)[0];
	var txt = elem.value;
	return txt;
}
	
function get_int_field(name) {
	return parseInt(get_field(name));
}

function write_error(num, txt, red=true) {
	elem = document.getElementById('sp{0}'.format(num));
	if (red) {
		elem.setAttribute('style', '');
	} else {
		elem.setAttribute('style', 'color: green');
	}
	elem.innerHTML = txt;
}

function clear_errors() {
	for (var i = 1; i <= 11; ++i) {
		if (i != 9) {
			write_error(i, "");
		}
	}
}

output = get_elem('log')

function get_elem(cls) {
	var element = document.getElementsByClassName(cls)[0];
	return element;
}

function write_log(s) {
	output.innerHTML = '<span> {0} </span> <br>'.format(s) + output.innerHTML;
}

function Operation(type, u, v, len) {
	this.type = type;
	this.u = u;
	this.v = v;
	this.len = len;
}

function Edge(u, v, len) {
	this.u = u;
	this.v = v;
	this.len = len;
	
	this.write = function() {
		write_log("{0} -> {1}, len = {2}".format(u, v, len));
	}
}

function Graph() {
	this.n = 1;
	this.edges = [];
	this.g = [];
	this.sz = [];
	
	this.par = [];
	this.has_heavy = [];
	this.is_heavy_up = [];
	
	this.add_edge = function(u, v, len) {
		this.edges.push(new Edge(u, v, len));
		if (v >= this.n) {
			this.n = v + 1;
		}
		if (u >= this.n) {
			this.n = u + 1;
		}
	}
	
	this.change_edge = function(u, v, len) {
		for (var i = 0; i < this.edges.length; ++i) {
			if (this.edges[i].u == u && this.edges[i].v == v) {
				this.edges[i].len = len;
				return true;
			}
		}
		return false;
	}
	
	this.get_edge = function(u, v) {
		for (var i = 0; i < this.g[u].length; ++i) {
			if (this.g[u][i].v == v) {
				return this.g[u][i].len;
			}
		}
	}
	
	this.get_list_of_adjecency = function() {
		this.g = [];
		this.sz = [];
		this.par = [];
		this.has_heavy = [];
		this.is_heavy = [];
		for (var i = 0; i < this.n; ++i) {
			this.g[i] = [];
			this.sz[i] = 0;
			this.par[i] = -1;
			this.has_heavy[i] = false;
			this.is_heavy_up[i] = false;
		}
		for (var i = 0; i < this.edges.length; ++i) {
			this.g[this.edges[i].u].push(new Edge(this.edges[i].u, this.edges[i].v, this.edges[i].len));
			this.g[this.edges[i].v].push(new Edge(this.edges[i].v, this.edges[i].u, this.edges[i].len));
		}
	}
	
	this.write = function() {
		write_log('Vertices: {0}'.format(this.n));
		for (var i = 0; i < this.n; ++i) {
			for (var j = 0; j < this.g[i].length; ++j) {
				this.g[i][j].write();
			}
		}
	}
}

function Operations() {
	this.all = [];
	this.cur = 0;
	this.length = 0;
	
	this.add_operation = function(type, u, v, len) {
		this.all[this.cur] = new Operation(type, u, v, len);
		this.cur += 1;
		this.length = this.cur;
	}
	
	this.get_graph = function get_graph() {
		g = new Graph();
		for (var i = 0; i < this.cur; ++i) {
			if (this.all[i].type == 'add') {
				g.add_edge(this.all[i].u, this.all[i].v, this.all[i].len);
			} else if (this.all[i].type == 'change') {
				g.change_edge(this.all[i].u, this.all[i].v, this.all[i].len);
			}
		}
		g.get_list_of_adjecency();
		return g;
	}
	
	this.get_info = function get_info(num) {
		if (this.all[num].type == 'add') {
			return 'Добавление ребра {0} -> {1}, len = {2}'.format(this.all[num].u, this.all[num].v, this.all[num].len);
		}
		if (this.all[num].type == 'change') {
			return 'Изменение веса ребра {0} -> {1}, len = {2}'.format(this.all[num].u, this.all[num].v, this.all[num].len);
		}
	}
	
	this.undo = function undo() {
		if (this.cur == 0) {
			return false;
		}
		this.cur -= 1;
		write_log('Отмена операции {0}'.format(this.get_info(this.cur)));
		draw_tree();
		return true;
	}
	
	this.redo = function redo() {
		if (this.cur == this.length) {
			return false;
		}
		write_log('Повторное применение операции {0}'.format(this.get_info(this.cur)));
		this.cur += 1;
		draw_tree();
		return true;
	}
}

function coords_from_str(str) {
	var attr = str.split(',');
	var x = parseInt(attr[0]), y = parseInt(attr[1]);
	return {x:x, y:y};
}

function update_coords(first, second, mask) {
	var dx = second.x - first.x;
	var dy = second.y - first.y;
	var dist = Math.sqrt(dx * dx + dy * dy);
	dxx = dx * sd_len;
	dyy = dy * sd_len;
	if (dist * sd_len > 30) {
		dxx = dx / dist * 30;
		dyy = dy / dist * 30;
	}
	if (mask & 1) {
		first.x += Math.floor(dxx);
		first.y += Math.floor(dyy);
	}
	if (mask & 2) {
		second.x -= Math.floor(dxx);
		second.y -= Math.floor(dyy);
	}
	return {first: first, second:second};
}

function Graphics() {
	this.nodes = {};
	this.edges = {};
	
	this.clear = function clear() {
		d3.select("svg").remove();
		this.svg = d3.select("body").select("div.canvas").append("svg");
		this.height = 400;
		this.width = 900;
		this.svg.attr("height", this.height)
			.attr("width", this.width);
	}
	
	this.check_size = function check_size(x, y) {
		if (this.width < x) {
			this.width = x;
		}
		if (this.height < y) {
			this.height = y + 5;
		}
		this.svg.attr("height", this.height)
			.attr("width", this.width);
	}
	this.clear();
	
	this.draw_node = function draw_vertex(v, x, y) {
		var g = this.svg.append("g")
			.attr("onclick", "node_onclick({0})".format(v));
		
		g.append("circle")
			.style("fill", "white")
			.style("stroke", "steelblue")
			.style("stroke-width", "2") 
			.attr("r", rad_vert)
			
		g.append("text")
			.attr("y", 5)
			.attr('text-anchor', 'middle')
			.attr("fill", "black")
			.style('font-weight', 'bold')
			.text('{0}'.format(v));
		
		this.nodes[v] = g._groups[0][0];
		this.set_node_coords(v, x, y);
		this.check_size(x + rad_vert, y + rad_vert);
	}
	
	this.get_node_coords = function(index) {
		console.log(this.nodes[index]);
		var attr = this.nodes[index].getAttribute("transform").substr(10).split(',');
		var x = parseInt(attr[0]), y = parseInt(attr[1]);
		return {x:x, y:y};
	}
	
	this.set_node_coords = function(index, x, y) {
		this.nodes[index].setAttribute("transform", "translate(" + x + ", " + y + ")");
	}
	
	this.set_node_col = function(index, col) {
		this.nodes[index].firstChild.style.fill = col;
	}
	
	this.set_node_text = function(index, txt) {
		this.nodes[index].childNodes[1].textContent = txt;
	}
	
	this.draw_edge = function draw_edge(u, v, len) {
		c1 = this.get_node_coords(u);
		c2 = this.get_node_coords(v);
		
		var g = this.svg.append("g")
			.attr("onclick", "edge_onclick({0}, {1})".format(u, v));
		
		g.append('path')
			.attr("d", "M{0},{1}L{2},{3}".format(c1.x, c1.y + rad_vert, c2.x, c2.y - rad_vert))
			.attr("stroke", "gray")
			.attr("stroke-width", width_light);
			
		g.append("text")
			.attr("x", "{0}".format(Math.round((c1.x + c2.x) / 2)))
			.attr("y", "{0}".format(Math.round((c1.y + c2.y) / 2) + 5))
			.attr('text-anchor', 'middle')
			.attr("fill", "black")
			.text("{0}".format(len));
		
		this.edges[[u, v]] = g._groups[0][0];
	}
	
	this.set_edge_stroke_width = function(u, v, width) {
		this.edges[[u, v]].childNodes[0].setAttribute('stroke-width', width);
	}
	
	this.set_col_edge = function(u, v, col) {
		this.edges[[u, v]].childNodes[0].setAttribute('stroke', col);
	}
	
	this.get_edge_coords = function(u, v) {
		var d = this.edges[[u, v]].childNodes[0].getAttribute('d');
		var first = d.substr(1, d.indexOf('L') - 1);
		var second = d.substr(d.indexOf('L') + 1);
		return {first:coords_from_str(first), second:coords_from_str(second)};
	}
	
	this.set_edge_coords = function(u, v, coords) {
		var first = coords.first;
		var second = coords.second;
		var d = "M{0},{1}L{2},{3}".format(first.x, first.y, second.x, second.y);
		this.edges[[u, v]].childNodes[0].setAttribute('d', d);
	}
	
	this.set_short_edge = function(u, v, mask) {
		var coords = this.get_edge_coords(u, v);
		var first = coords.first;
		var second = coords.second;
		this.set_edge_coords(u, v, update_coords(first, second, mask));
	}
	
	this.original_edge = function(u, v, mask) {
		if ([u, v] in this.edges) {
			first = this.get_node_coords(u);
			second = this.get_node_coords(v);
			first.y += rad_vert;
			second.y -= rad_vert;
			this.set_edge_coords(u, v, update_coords(first, second, 0));
		}
	}
}

function build_tree(g, v, par, level, min_left) {
	var x = min_left;
	var is_leaf = true;
	for (var i = 0; i < g[v].length; ++i) {
		var to = g[v][i].v;
		if (to == par) {
			continue;
		}
		min_left = build_tree(g, to, v, level + 1, min_left);
		is_leaf = false;
	}
	if (!is_leaf) {
		min_left -= size_x_between_vertices;
	}
	x += min_left;
	x = Math.round(x / 2);
	graphics.draw_node(v, x, start_y + level * size_y_between_vertices);
	for (var i = 0; i < g[v].length; ++i) {
		var to = g[v][i].v;
		if (to == par) {
			continue;
		}
		graphics.draw_edge(v, to, g[v][i].len);
	}
	return Math.max(x, min_left) + size_x_between_vertices;
}

function draw_tree() {
	graphics.clear();
	var g = operations.get_graph();
	build_tree(g.g, 0, -1, 0, start_x);
	calculated_sizes = false;
}

operations = new Operations();
graphics = new Graphics();
draw_tree();
visualisation = 0;
last_action = '';

function get_delay() {
	res = get_int_field('speed');
	return (101 - res) * 10;
	return default_delay;
}

function Visualisation() {
	this.all = [];
	this.cur = 0;
	
	this.add_item = function add_item(type, v, value, delay=true) {
		this.all.push([type, v, value, delay]);
	}
	
	this.show = function(show_all=false) {
		if (show_all) {
			draw_tree();
			for (var num = 0; num < this.all.length; ++num) {
				if (this.all[num][0] == 'col') {
					graphics.set_node_col(this.all[num][1], this.all[num][2]);
				} else if (this.all[num][0] == 'text') {
					graphics.set_node_text(this.all[num][1], this.all[num][2]);
				} else if (this.all[num][0] == 'strong_edge') {
					graphics.set_edge_stroke_width (this.all[num][1][0], this.all[num][1][1], this.all[num][2]);
				} else if (this.all[num][0] == 'col_edge') {
					graphics.set_col_edge(this.all[num][1][0], this.all[num][1][1], this.all[num][2]);
				} else if (this.all[num][0] == 'short_edge') {
					graphics.set_short_edge(this.all[num][1][0], this.all[num][1][1], this.all[num][2]);
				}
			}
			return;
		}
		pre_vis.show(true);
		for (var num = 0; num < this.cur; ++num) {
			if (this.all[num][0] == 'col') {
				graphics.set_node_col(this.all[num][1], this.all[num][2]);
			} else if (this.all[num][0] == 'text') {
				graphics.set_node_text(this.all[num][1], this.all[num][2]);
			} else if (this.all[num][0] == 'strong_edge') {
				graphics.set_edge_stroke_width (this.all[num][1][0], this.all[num][1][1], this.all[num][2]);
			} else if (this.all[num][0] == 'col_edge') {
				graphics.set_col_edge(this.all[num][1][0], this.all[num][1][1], this.all[num][2]);
			} else if (this.all[num][0] == 'short_edge') {
				graphics.set_short_edge(this.all[num][1][0], this.all[num][1][1], this.all[num][2]);
			}
		}
	}
	
	this.forward = function() {
		while (this.cur < this.all.length) {
			this.cur += 1;
			delay = this.all[this.cur - 1][3]
			if (delay) {
				break;
			}
		}
		this.show();
		if (this.cur == this.all.length) {
			visualisation = 0;
			last_action = '';
		}
	}
	
	this.backward = function() {
		if (this.cur == 0) {
			return false;
		}
		while (this.cur >= 0) {
			this.cur -= 1;
			if (this.cur > 0 && !this.all[this.cur - 1][3]) {
				continue;
			}
			break;
		}
		this.show();
		return true;
	}
}

final_message = '';

function visualize(vis, with_vis, num=0) {
	if (num >= vis.all.length) {
		visualisation = 0;
		if (with_vis) {
			last_action = '';
		}
		clear_errors();
		calculated_sizes = true;
		return;
	}
	if (vis.all[num][0] == 'message') {
		write_log(vis.all[num][1]);
		visualize(vis, with_vis, num + 1);
		return;
	}
	if (vis.all[num][0] == 'col') {
		graphics.set_node_col(vis.all[num][1], vis.all[num][2]);
	} else if (vis.all[num][0] == 'text') {
		graphics.set_node_text(vis.all[num][1], vis.all[num][2]);
	} else if (vis.all[num][0] == 'strong_edge') {
		graphics.set_edge_stroke_width (vis.all[num][1][0], vis.all[num][1][1], vis.all[num][2]);
	} else if (vis.all[num][0] == 'col_edge') {
		graphics.set_col_edge(vis.all[num][1][0], vis.all[num][1][1], vis.all[num][2]);
	} else if (vis.all[num][0] == 'short_edge') {
		graphics.set_short_edge(vis.all[num][1][0], vis.all[num][1][1], vis.all[num][2]);
	}
	delay = vis.all[num][3]
	if (delay && with_vis) {
		setTimeout(function() { visualize(vis, with_vis, num + 1) }, get_delay());
	} else {
		visualize(vis, with_vis, num + 1);
	}
}
	
function calculate_sizes_visualisation(g, v, par, vis) {
	vis.add_item('col', v, 'blue');
	var sz = 1;
	var sizes = []
	for (var i = 0; i < g[v].length; ++i) {
		var to = g[v][i].v;
		if (to != par) {
			to_sz = calculate_sizes_visualisation(g, to, v, vis);
			sz += to_sz;
			sizes.push(to_sz);
		} else {
			sizes.push(0);
		}
	}
	vis.add_item('col', v, '#00ff00', false);
	for (var i = 0; i < g[v].length; ++i) {
		var to = g[v][i].v;
		if (sz <= 2 * sizes[i]) {
			vis.add_item('strong_edge', [v, to], width_heavy, false);
		}
	}
	vis.add_item('text', v, '{0}'.format(sz));
	return sz;
}

function calculate_pathes(g, v, par) {
	g.par[v] = par;
	var sz = 1;
	var sizes = []
	for (var i = 0; i < g.g[v].length; ++i) {
		var to = g.g[v][i].v;
		if (to != par) {
			to_sz = calculate_pathes(g, to, v);
			sz += to_sz;
			sizes.push(to_sz);
		} else {
			sizes.push(0);
		}
	}
	for (var i = 0; i < g.g[v].length; ++i) {
		var to = g.g[v][i].v;
		if (sz <= 2 * sizes[i]) {
			g.has_heavy[v] = true;
			g.is_heavy_up[to] = true;
		}
	}
	return sz;
}

function go_up(g, v, vis) {
	var path = [];
	while (v != 0) {
		path.push(v);
		if (!g.is_heavy_up[v]) {
			break;
		}
		v = g.par[v];
	}
	if (path.length > 0) {
		path.push(g.par[path[path.length - 1]]);
	}
	if (path.length > 0) {
		vis.add_item('col', path[0], '#00ffff');
		for (var i = 0; i + 1 < path.length; ++i) {
			vis.add_item('col_edge', [path[i + 1], path[i]], '#ffff00');
		}
		for (var i = 0; i + 1 < path.length; ++i) {
			vis.add_item('col_edge', [path[i + 1], path[i]], 'gray', false);
			var mask = 0;
			if (i == 0) {
				mask |= 2;
			}
			if (i + 2 == path.length) {
				mask |= 1;
			}
			vis.add_item('short_edge', [path[i + 1], path[i]], mask, false);
		}
		vis.add_item('col', path[0], '#00ff00');
	}
}

function original_edges(g) {
	for (var i = 0; i < g.n; ++i) {
		for (var j = 0; j < g.g[i].length; ++j) {
			graphics.original_edge(i, g.g[i][j].v);
		}
	}
}

var vis = new Visualisation();
var pre_vis = new Visualisation();

function divide_on_paths() {
	var g = operations.get_graph();
	original_edges(g);
	vis = new Visualisation();
	vis.add_item('message', 'Выделение путей...', '');
	calculate_pathes(g, 0, -1);
	for (var i = 0; i < g.n; ++i) {
		if (!g.has_heavy[i]) {
			go_up(g, i, vis);
		}
	}
	vis.add_item('message', 'Все пути были успешно выделены', '');
	return vis;
}

function standard_graph(g) {
	for (var i = 0; i < g.n; ++i) {
		graphics.set_node_col(i, 'white');
	}
	for (var i = 0; i < g.edges.length; ++i) {
		graphics.set_edge_stroke_width(g.edges[i].u, g.edges[i].v, width_light);
	}
	original_edges(g);
}

function calculate_subtree_sizes() {
	var g = operations.get_graph();
	standard_graph(g);
	vis = new Visualisation();
	vis.add_item('message', 'Подсчет размеров поддеревьев...', '');
	calculate_sizes_visualisation(g.g, 0, -1, vis);	
	vis.add_item('message', 'Размеры поддеревьев успешно посчитаны', '');
	return vis;
}

function visualisation_type() {
	if (get_field('select_v') == 'Ручная') {
		return 2;
	}
	return 1;
}

function do_visualize(vis, with_vis=true) {
	if (!with_vis) {
		visualize(vis, with_vis);
		return;
	}
	visualisation = visualisation_type();
	if (visualisation == 1) {
		setTimeout(function() { visualize(vis, with_vis) }, get_delay());
	} else {
		vis.show();
	}
}

function combine(vis1, vis2) {
	res = new Visualisation();
	for (var i = 0; i < vis1.all.length; ++i) {
		res.all.push(vis1.all[i]);
	}
	for (var i = 0; i < vis2.all.length; ++i) {
		res.all.push(vis2.all[i]);
	}
	return res;
}

function build_decomposition_return(with_vis=true) {
	//draw_tree();
	vis1 = calculate_subtree_sizes();
	vis2 = divide_on_paths();
	return combine(vis1, vis2);
}

function build_decomposition(with_vis=true) {
	//draw_tree();
	vis1 = calculate_subtree_sizes();
	vis2 = divide_on_paths();
	if (!with_vis) {
		do_visualize(combine(vis1, vis2), with_vis);
	} else {
		vis = combine(vis1, vis2);
		do_visualize(vis, with_vis);
	}
}

function add_edge(v, len) {
	var g = operations.get_graph();
	operations.add_operation('add', v, g.n, len);
	write_log('Добавлено ребро {0} -> {1} длины {2}'.format(v, g.n, len));
	draw_tree();
}

function change_edge(u, v, len) {
	operations.add_operation('change', u, v, len);
	write_log('Длина ребра {0} -> {1} изменена на длину {2}'.format(u, v, len));
	draw_tree();
}

var select_node = 0;
var selected_nodes = []
var last_action = '';
var last_action_parameters = []

function add_edge_onclick() {
	clear_errors();
	if (visualisation) {
		write_error(1, 'Дождитесь окончания другой операции.');
		return;
	}
	if (!is_int('add_edge_value')) {
		write_error(1, 'Введите целое число от 1 до {0}'.format(max_len));
		return;
	}
	var len = get_int_field('add_edge_value');
	if (len > max_len || len <= 0) {
		write_error(1, 'Введите целое число от 1 до {0}'.format(max_len));
		return;
	}
	select_node = 1;
	selected_nodes = [];
	last_action = 'add_edge';
	last_action_parameters = [len];
	write_error(1, 'Выберите вершину', false);
}

function change_edge_onclick() {
	clear_errors();
	if (visualisation) {
		write_error(8, 'Дождитесь окончания другой операции.');
		return;
	}
	if (!is_int('change_edge_value')) {
		write_error(8, 'Введите целое число от 1 до {0}'.format(max_len));
		return;
	}
	var len = get_int_field('change_edge_value');
	if (len > max_len || len <= 0) {
		write_error(8, 'Введите целое число от 1 до {0}'.format(max_len));
		return;
	}
	select_node = 1;
	selected_nodes = [];
	last_action = 'change_edge';
	last_action_parameters = [len];
	write_error(8, 'Выберите ребро', false);
}

function node_onclick(v) {
	if (visualisation) {
		return;
	}
	if (selected_nodes.length < select_node) {
		selected_nodes.push(v);
		if (last_action == 'get_max') {
			graphics.set_node_col(v, '#00ffff');
		}
	}
	if (selected_nodes.length == select_node) {
		if (last_action == 'add_edge') {
			add_edge(v, last_action_parameters[0]);
		} else if (last_action == 'get_max') {
			get_max(selected_nodes[0], selected_nodes[1]);
		}
		select_node = 0;
		clear_errors();
	}
}

function edge_onclick(u, v) {
	if (visualisation) {
		return;
	}
	if (selected_nodes.length < select_node) {
		selected_nodes.push(v);
	}
	if (selected_nodes.length == select_node) {
		if (last_action == 'change_edge') {
			change_edge(u, v, last_action_parameters[0]);
		}
		select_node = 0;
		clear_errors();
	}
}

function add_random_edge_onclick() {
	clear_errors();
	if (visualisation) {
		write_error(2, 'Дождитесь окончания другой операции.');
		return;
	}
	var g = operations.get_graph();
	add_edge(rand_int(g.n), rand_int(max_len + 1));
}

function calculate_subtree_sizes_onclick() {
	clear_errors();
	if (visualisation) {
		write_error(4, 'Дождитесь окончания другой операции.');
		return;
	}
	do_visualize(calculate_subtree_sizes());
}

function load_sample_onclick() {
	clear_errors();
	if (visualisation) {
		write_error(3, 'Дождитесь окончания другой операции.');
		return;
	}
	var sample = get_field('select_sample');
	load_sample(sample);
}

function divide_on_paths_onclick() {
	clear_errors();
	if (visualisation) {
		write_error(5, 'Дождитесь окончания другой операции.');
		return;
	}
	if (!calculated_sizes && false) {
		write_error(5, 'Посчитаейте размеры поддеревьев.');
		return;
	}
	do_visualize(divide_on_paths());
}

function build_decomposition_onclick() {
	clear_errors();
	if (visualisation) {
		write_error(6, 'Дождитесь окончания другой операции.');
		return;
	}
	build_decomposition();
}

function undo_onclick() {
	clear_errors();
	if (visualisation == 1) {
		write_error(10, 'Дождитесь окончания другой операции.');
		return;
	}
	if (visualisation == 0) {
		write_error(10, 'Визуализация не начата');
		return;
	}
	if (!vis.backward()) {
		write_error(10, 'Визуализация только началась');
		return;
	}
}

function redo_onclick() {
	clear_errors();
	if (visualisation == 1) {
		write_error(10, 'Дождитесь окончания другой операции.');
		return;
	}
	if (visualisation == 0) {
		write_error(10, 'Визуализация не начата');
		return;
	}
	vis.forward();
}

function get_max_onclick() {
	clear_errors();
	if (visualisation) {
		write_error(7, 'Дождитесь окончания другой операции.');
		return;
	}
	build_decomposition(false);
	select_node = 2;
	selected_nodes = [];
	last_action = 'get_max';
	write_error(7, 'Выберите 2 вершины', false);
}

function get_ancestors(v, g) {
	var res = [];
	while (v != -1) {
		res.push(v);
		v = g.par[v];
	}
	res.reverse();
	return res;
}

function get_lca(u, v, g) {
	a1 = get_ancestors(u, g);
	a2 = get_ancestors(v, g);
	pos1 = a1.length - 1;
	pos2 = a2.length - 1;
	while (a1[pos1] != a2[pos2]) {
		if (pos1 > pos2) {
			--pos1;
		} else {
			--pos2;
		}
	}
	return a1[pos1];
}

function update_max(v, lca, g, mx, mxu, mxv, vis) {
	while (v != lca) {
		var path = [];
		while (v != lca) {
			path.push(v);
			if (!g.is_heavy_up[v]) {
				break;
			}
			v = g.par[v];
		}
		if (path.length > 0) {
			path.push(g.par[path[path.length - 1]]);
		}
		if (path.length > 0) {
			var best_pos = -1, best_len = -1;
			for (var i = 0; i + 1 < path.length; ++i) {
				var cur_len = g.get_edge(path[i + 1], path[i]);
				vis.add_item('col_edge', [path[i + 1], path[i]], '#ffff00', (i + 2 == path.length));
				if (cur_len > best_len) {
					best_len = cur_len;
					best_pos = i;
				}
			}
			for (var i = 0; i + 1 < path.length; ++i) {
				vis.add_item('col_edge', [path[i + 1], path[i]], 'gray', false);
			}
			if (best_len > mx) {
				if (mxu != -1) {
					vis.add_item('col_edge', [mxu, mxv], 'gray', false);
				}
				vis.add_item('col_edge', [path[best_pos + 1], path[best_pos]], '#00ff00', true);
				mx = best_len;
				mxu = path[best_pos + 1];
				mxv = path[best_pos];
			}
		}
		if (v == lca) {
			break;
		}
		v = g.par[v];
	}
	return [mx, mxu, mxv];
}

function get_max(u, v) {
	var g = operations.get_graph();
	calculate_pathes(g, 0, -1);
	pre_vis = build_decomposition_return();
	vis = new Visualisation();
	vis.add_item('message', 'Поиск максимума между вершинами {0} и {1}'.format(u, v), '');
	vis.add_item('col', u, '#00ffff', false);
	vis.add_item('col', v, '#00ffff');
	lca = get_lca(u, v, g);
	vis.add_item('col', lca, '#ffff00');
	var mx = 0;
	var mxu = -1;
	var mxv = -1;
	res = update_max(u, lca, g, mx, mxu, mxv, vis);
	mx = res[0];
	mxu = res[1];
	mxv = res[2];
	res = update_max(v, lca, g, mx, mxu, mxv, vis);
	mx = res[0];
	mxu = res[1];
	mxv = res[2];
	vis.add_item('col', lca, '#00ff00');
	vis.add_item('message', 'Максимальное ребро - {0} -> {1}, len = {2}'.format(mxu, mxv, mx), '');
	do_visualize(pre_vis, false);
	do_visualize(vis);
}

var VINT = 0, CURI = 0;

function visualize_querries(querries) {
	if (visualisation) {
		return;
	}
	if (CURI == querries.length) {
		clearInterval(VINT);
		draw_tree();
		build_decomposition(false);
		return;
	}
	var i = CURI;
	CURI += 1;
	if (querries[i][0] == 'update') {
		operations.add_operation('change', querries[i][1], querries[i][2], querries[i][3]);
		draw_tree();
		build_decomposition(false);
		write_log('Изменение веса ребра ({0}, {1}) на {2}'.format(querries[i][1], querries[i][2], querries[i][3]));
	} else {
		get_max(querries[i][1], querries[i][2]);
		write_log('Поискк максимума между вершинами {0} {1}'.format(querries[i][1], querries[i][2]));
	}
}

function go_test() {
	clear_errors();
	if (visualisation) {
		write_error(11, 'Дождитесь окончания другой операции.');
		return;
	}
	visualisation = 1;
	var s = get_field('test_area');
	var res = parse_test(s);
	if (res[0] != "") {
		write_error(11, res[0]);
		visualisation = 0;
		return;
	}
	go_to_the_top();
	operations = new Operations();
	var graph = res[1][0];
	var querries = res[1][1];
	for (var i = 0; i < graph.length; ++i) {
		operations.add_operation('add', graph[i][0], graph[i][1], graph[i][2]);
	}
	draw_tree();
	build_decomposition();
	CURI = 0;
	VINT = setInterval(function() {
		visualize_querries(querries);
	});
}