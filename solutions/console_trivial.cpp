#include <bits/stdc++.h>

using namespace std;

struct query {
    enum type_of_query {
        get_max,
        update
    };
    int from, to, dist;
    type_of_query tp;
    query() {
    }
    query(int from, int to): from(from), to(to), tp(type_of_query::get_max) {
    }
    query(int from, int to, int dist): from(from), to(to), dist(dist), tp(type_of_query::update) {
    }
};

int n;
vector<int> dist, from, to;
vector<query> querries;
map<pair<int, int>, int> all_edges;

#include "input.h"

vector<vector<int>> g;

int find_max(int v, int last, int par = -1) {
    if (v == last) {
        return -1;
    }
    for (int i = 0; i < g[v].size(); ++i) {
        int num = g[v][i];
        int nxt = v ^ from[num] ^ to[num];
        if (nxt == par) {
            continue;
        }
        int res = find_max(nxt, last, v);
        if (res != -2) {
            res = max(res, dist[num]);
            return res;
        }
    }
    return -2;
}

void solve_trivial() {
    g.resize(n + 5);
    for (int i = 2; i <= n; ++i) {
        g[from[i]].push_back(i);
        g[to[i]].push_back(i);
    }
    for (const query &q : querries) {
        if (q.tp == query::type_of_query::update) {
            dist[all_edges[make_pair(q.from, q.to)]] = q.dist;
        } else {
            int mx = find_max(q.from, q.to);
            cout << mx << endl;
        }
    }
}

int main() {
    try_input();
    solve_trivial();
    return 0;
}
