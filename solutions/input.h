#include <bits/stdc++.h>

using namespace std;

map<string, int> parameters;

void load_parameters() {
    ifstream in("config");
    string name;
    int val;
    while (in >> name >> val) {
        parameters[name] = val;
    }
    in.close();
}

string int_to_str(int x) {
    if (x == 0) {
        return "";
    }
    string res = "";
    while (x) {
        res += x % 10 + '0';
        x /= 10;
    }
    reverse(res.begin(), res.end());
    return res;
}

string error(int lines, string message) {
    return "Error, line " + int_to_str(lines) + "\n" + message;
}

string interval_error(int lines, string param, int l, int r) {
    return error(lines, "Parameter '" + param + "' should be from " + int_to_str(l) + " to " + int_to_str(r));
}

bool check_interval(int l, int r, int x) {
    return l <= x && x <= r;
}

string input_data(istream &in = cin) {
    load_parameters();
    const int MAX_N = parameters["MAX_N"];
    const int MAX_Q = parameters["MAX_Q"];
    const int MAX_DIST = parameters["MAX_DIST"];
    int lines = 1;
    if (!(in >> n)) {
        return error(lines, "Should contain one integer 'n' - number of vertices");
    }
    if (!check_interval(2, MAX_N, n)) {
        return interval_error(lines, "n", 2, MAX_N);
    }
    dist.resize(n + 5);
    from.resize(n + 5);
    to.resize(n + 5);
    lines += 1;
    for (int i = 2; i <= n; ++i) {
        int par, d;
        if (!(in >> par >> d)) {
            return error(lines, "Should contain two integers describing edge 'parent', 'dist' - parent of new vertex, and length of the edge");
        }
        if (!check_interval(1, i - 1, par)) {
            return interval_error(lines, "parent", 1, i - 1);
        }
        if (!check_interval(1, MAX_DIST, d)) {
            return interval_error(lines, "dist", 1, MAX_DIST);
        }
        lines += 1;
        dist[i] = d;
        from[i] = par;
        to[i] = i;
        all_edges[make_pair(from[i], to[i])] = i;
    }
    int q;
    if (!(in >> q)) {
        return error(lines, "Should contain one integer 'q' - number of querries");
    }
    if (!check_interval(1, MAX_Q, q)) {
        return interval_error(lines, "q", 1, MAX_Q);
    }
    lines += 1;
    querries.resize(q);
    for (int i = 0; i < q; ++i) {
        string tp;
        if (!(in >> tp)) {
            return error(lines, "Should contain one string 'tp' - type of the query");
        }
        if (tp != "update" && tp != "getmax") {
            return error(lines, "Parameter 'tp' should be 'update' or 'getmax'");
        }
        if (tp == "update") {
            int from, to, dist;
            if (!(in >> from >> to >> dist)) {
                return error(lines, "Should contain three integers 'from', 'to', 'dist'");
            }
            if (!check_interval(1, n, from)) {
                return interval_error(lines, "from", 1, n);
            }
            if (!check_interval(1, n, to)) {
                return interval_error(lines, "to", 1, n);
            }
            if (!check_interval(1, MAX_DIST, dist)) {
                return interval_error(lines, "dist", 1, MAX_DIST);
            }
            if (from >= to) {
                return error(lines, "Parameter 'from' should be less than 'to'");
            }
            if (!all_edges.count(make_pair(from, to))) {
                return error(lines, "Specified edge doesn't exist in the tree");
            }
            querries[i] = query(from, to, dist);
        } else {
            int from, to;
            if (!(in >> from >> to)) {
                return error(lines, "Should contain two integers 'from', 'to'");
            }
            if (!check_interval(1, n, from)) {
                return interval_error(lines, "from", 1, n);
            }
            if (!check_interval(1, n, to)) {
                return interval_error(lines, "to", 1, n);
            }
            if (from == to) {
                return error(lines, "Parameter 'from' should not be equal 'to'");
            }
            querries[i] = query(from, to);
        }
        lines += 1;
    }
    return "";
}

void try_input() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    string bad_input;
    if (false) {
        ifstream in("../tests/001.in");
        bad_input = input_data(in);
    } else {
        bad_input = input_data();
    }
    if (bad_input != "") {
        cout << bad_input << endl;
        exit(0);
    }
}
