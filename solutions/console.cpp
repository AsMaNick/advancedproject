#include <bits/stdc++.h>

using namespace std;

class tree {
public:
    int sz;
    tree() {
    }
    tree(int Sz) {
        mx.resize(Sz * 4);
        sz = Sz;
    }
    void update(int x, int value) {
        update(1, 0, sz - 1, x, value);
    }
    int get_max(int l) {
        return get_max(1, 0, sz - 1, l, sz - 1);
    }
    int get_max(int l, int r) {
        return get_max(1, 0, sz - 1, l, r);
    }

private:
    void update(int v, int tl, int tr, int x, int value) {
        if (tl == tr) {
            mx[v] = value;
            return;
        }
        int mid = (tl + tr) / 2;
        if (x <= mid) {
            update(2 * v, tl, mid, x, value);
        } else {
            update(2 * v + 1, mid + 1, tr, x, value);
        }
        mx[v] = max(mx[2 * v], mx[2 * v + 1]);
    }
    int get_max(int v, int tl, int tr, int l, int r) {
        if (l > r) {
            return -1;
        }
        if (tl == l && tr == r) {
            return mx[v];
        }
        int mid = (tl + tr) / 2;
        if (r <= mid) {
            return get_max(2 * v, tl, mid, l, r);
        }
        if (l > mid) {
            return get_max(2 * v + 1, mid + 1, tr, l, r);
        }
        return max(get_max(2 * v, tl, mid, l, mid), get_max(2 * v + 1, mid + 1, tr, mid + 1, r));
    }
    vector<int> mx;
};

struct query {
    enum type_of_query {
        get_max,
        update
    };
    int from, to, dist;
    type_of_query tp;
    query() {
    }
    query(int from, int to): from(from), to(to), tp(type_of_query::get_max) {
    }
    query(int from, int to, int dist): from(from), to(to), dist(dist), tp(type_of_query::update) {
    }
};

int n;
vector<int> dist, from, to;
vector<query> querries;
map<pair<int, int>, int> all_edges;

#include "input.h"

int curt;
vector<bool> has_heavy, heavy_par;
vector<int> tin, tout, sz;
vector<vector<int>> g, parent;
vector<tree> all_paths;
vector<int> path_num, pos_in_path, last_in_path;

void init() {
    g.resize(n + 5);
    sz.resize(n + 5);
    tin.resize(n + 5);
    tout.resize(n + 5);
    has_heavy.resize(n + 5);
    heavy_par.resize(n + 5);
    path_num.resize(n + 5);
    pos_in_path.resize(n + 5);
    last_in_path.resize(n + 5);
    all_paths.clear();
    int pw = 1, levs = 0;
    while (pw <= n) {
        pw *= 2;
        levs += 1;
    }
    parent.resize(levs, vector<int> (n + 5));
}

void dfs(int v) {
    tin[v] = curt;
    ++curt;
    sz[v] = 1;
    for (int i = 0; i < g[v].size(); ++i) {
        dfs(g[v][i]);
        sz[v] += sz[g[v][i]];
    }
    for (int i = 0; i < g[v].size(); ++i) {
        if (sz[v] <= 2 * sz[g[v][i]]) {
            has_heavy[v] = true;
            heavy_par[g[v][i]] = true;
        }
    }
    tout[v] = curt;
    ++curt;
}

bool is_ancestor(int v1, int v2) {
    return (v1 == 0) || (tin[v1] <= tin[v2] && tout[v1] >= tout[v2]);
}

void get_all_ancestors() {
    for (int lev = 1; lev < parent.size(); ++lev) {
        for (int i = 1; i <= n; ++i) {
            parent[lev][i] = parent[lev - 1][parent[lev - 1][i]];
        }
    }
}

void go_up(int v) {
    vector<int> path;
    while (v != 1) {
        path.push_back(v);
        if (!heavy_par[v]) {
            break;
        }
        v = parent[0][v];
    }
    if (path.size() == 0) {
        return;
    }
    /*cout << all_paths.size() << ") ";
    for (int i = 0; i < path.size(); ++i) {
        cout << path[i] << " ";
    }
    cout << endl;*/
    tree t(path.size());
    for (int i = 0; i < path.size(); ++i) {
        path_num[path[i]] = all_paths.size();
        pos_in_path[path[i]] = i;
        t.update(i, dist[path[i]]);
    }
    last_in_path[all_paths.size()] = path.back();
    all_paths.push_back(t);
}

void do_heavy_light() {
    for (int i = 1; i <= n; ++i) {
        if (!has_heavy[i]) {
            go_up(i);
        }
    }
    path_num[1] = -1;
}

int lca(int v1, int v2) {
    if (is_ancestor(v1, v2)) {
        return v1;
    }
    if (is_ancestor(v2, v1)) {
        return v2;
    }
    for (int i = parent.size() - 1; i >= 0; --i) {
        if (!is_ancestor(parent[i][v1], v2)) {
            v1 = parent[i][v1];
        }
    }
    return parent[0][v1];
}

void update(int v, int d) {
    if (v != 1) {
        all_paths[path_num[v]].update(pos_in_path[v], d);
    }
}

int find_max_vert(int v, int lc) {
    int res = -1;
    while (v != lc) {
        if (path_num[v] != path_num[lc]) {
            res = max(res, all_paths[path_num[v]].get_max(pos_in_path[v]));
            v = parent[0][last_in_path[path_num[v]]];
        } else {
            res = max(res, all_paths[path_num[v]].get_max(pos_in_path[v], pos_in_path[lc] - 1));
            break;
        }
    }
    return res;
}

int find_max(int v1, int v2) {
    int lc = lca(v1, v2);
    return max(find_max_vert(v1, lc), find_max_vert(v2, lc));
}

void solve() {
    init();
    for (int i = 2; i <= n; ++i) {
        g[from[i]].push_back(to[i]);
        parent[0][to[i]] = from[i];
    }
    dfs(1);
    get_all_ancestors();
    do_heavy_light();
    for (const query &q : querries) {
        if (q.tp == query::type_of_query::update) {
            update(q.to, q.dist);
        } else {
            int mx = find_max(q.from, q.to);
            cout << mx << endl;
        }
    }
}

int main() {
    try_input();
    solve();
    return 0;
}
