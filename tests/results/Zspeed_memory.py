import matplotlib.pyplot as plt
import math


def build(leg, y_lab, x, y):
    plt.xlabel('N parameter')
    plt.ylabel(y_lab)
    plt.ylim(0, max(y) * 1.2)
    xx = []
    yy = []
    for i in range(0, len(x), 2):
        xx.append(x[i])
        yy.append((y[i] + y[i + 1]) / 2)
    print(yy)
    plt.plot(xx, yy)
    plt.scatter(xx, yy)
    plt.show()


def time_coefficient(arr):
    op_per_sec = 10 ** 8
    total_op = arr[1] / 1000 * op_per_sec
    assumed_op = arr[0] * (math.log(arr[0], 2) ** 2)
    return arr[0] * math.log(arr[0]) * 4 / (2 ** 20)
    return total_op / assumed_op


all_n = []
all_time = []
all_memory = []
for line in open('speed_memory.txt', 'r'):
    arr = list(map(int, line.split(' ')))
    all_n.append(arr[0])
    all_time.append(arr[1])
    all_memory.append(arr[2])
    print(arr, time_coefficient(arr))
build('speed', 'Speed, ms', all_n, all_time)
build('memory', 'Memory, MB', all_n, all_memory)
