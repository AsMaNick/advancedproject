#include <bits/stdc++.h>
#include "testlib.h"

using namespace std;

int n, q, max_weight;
vector<int> us, vs, weights;

void add_edge(int u, int v, int weight) {
    us.push_back(u);
    vs.push_back(v);
    weights.push_back(weight);
}

void gen_rand_tree() {
    for (int i = 1; i < n; ++i) {
        int u = rnd.next(1, i);
        int v = i + 1;
        int weight = rnd.next(1, max_weight);
        add_edge(u, v, weight);
    }
}

void gen_bin_tree() {
    for (int i = 1; i < n; ++i) {
        int u = (i + 1) / 2;
        int v = i + 1;
        int weight = rnd.next(1, max_weight);
        add_edge(u, v, weight);
    }
}

void gen_star() {
    for (int i = 1; i < n; ++i) {
        int u = 1;
        int v = i + 1;
        int weight = rnd.next(1, max_weight);
        add_edge(u, v, weight);
    }
}

void gen_bamboo() {
    for (int i = 1; i < n; ++i) {
        int u = i;
        int v = i + 1;
        int weight = rnd.next(1, max_weight);
        add_edge(u, v, weight);
    }
}

void gen_tree(string tp) {
    if (tp == "rnd") {
        gen_rand_tree();
    } else if (tp == "bin") {
        gen_bin_tree();
    } else if (tp == "star") {
        gen_star();
    } else if (tp == "bamboo") {
        gen_bamboo();
    } else {
        cout << "Incorrect type of tree" << endl;
        exit(1);
    }
}

void gen_querries(int updates, int getmaxs) {
    cout << q << endl;
    for (int i = 0; i < q; ++i) {
        if (rnd.next(updates + getmaxs) < updates) {
            int num = rnd.next(us.size());
            cout << "update " << us[num] << " " << vs[num] << " " << rnd.next(1, max_weight) << endl;
        } else {
            int u = rnd.next(1, n);
            int v = rnd.next(1, n);
            while (u == v) {
                u = rnd.next(1, n);
                v = rnd.next(1, n);
            }
            cout << "getmax " << u << " " << v << endl;
        }
    }
}

int main(int argc, char *argv[]) {
    registerGen(argc, argv, 1);
    string tp = argv[1];
    n = rnd.next(atoi(argv[2]), atoi(argv[3]));
    q = rnd.next(atoi(argv[4]), atoi(argv[5]));
    max_weight = rnd.next(1, atoi(argv[6]));
    gen_tree(tp);
    cout << n << endl;
    for (int i = 0; i < us.size(); ++i) {
        cout << us[i] << " " << weights[i] << endl;
    }
    gen_querries(atoi(argv[7]), atoi(argv[8]));
    return 0;
}
